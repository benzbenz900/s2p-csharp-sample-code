<?php
include_once 'class/PayApi.php';

// use Class

$response = array();
$useClass = new PayApi((object) array(
    "authorization" => "keyclient",
    "key"=>"secertkey",
));

// autoqrcode
$response['autoqrcode'] = $useClass->autoqrcode(
    array(
        "orderid"=>"string",
        "account"=>"string",
        "price"=>0
    )
);


// deposit
$response['deposit'] = $useClass->deposit(
    array(
        "orderid"=>"string",
        "price"=>0,
        "account"=>"string",
        "from_account"=>"string",
        "from_bank"=>"BBL",
        "from_name"=>"string",
    )
);

// withdraw
$response['withdraw'] = $useClass->withdraw(
    array(
        "orderid"=>"string",
        "price"=>0,
        "account"=>"string",
        "to_banking"=>"BBL",
        "name"=>"string",
    )
);


echo json_encode($response,JSON_PRETTY_PRINT);