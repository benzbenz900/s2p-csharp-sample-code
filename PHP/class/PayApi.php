<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class PayApi
{
    public function __construct($option) {
        $this->authorization = $option->authorization ;
        $this->key=$option->key;
        $this->data = array();
    }

    private function sendData(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->data->url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $this->data->playload,
        CURLOPT_HTTPHEADER => array(
            "authorization:".$this->authorization,
            "Content-Type: application/json"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response,true);
    }

    private function genSignature($option)
    {
        ksort($option);

        $signature = md5(
            http_build_query($option).'&key='.$this->key
        );

        parse_str(
            http_build_query($option).'&signature='.$signature,
            $playload
        );

        return json_encode($playload,JSON_NUMERIC_CHECK );
    }

    public function autoqrcode($option)
    {
        $this->data = (object) array(
            'url'=>'https://api.real-pay.co/api/autoqrcode',
            'playload' => $this->genSignature($option)
        );

        return $this->sendData();
    }

    public function deposit($option)
    {
        $this->data = (object) array(
            'url'=>'https://api.real-pay.co/api/deposit',
            'playload' => $this->genSignature($option)
        );

        return $this->sendData();
    }

    public function withdraw($option)
    {
        $this->data = (object) array(
            'url'=>'https://api.real-pay.co/api/withdraw',
            'playload' => $this->genSignature($option)
        );

        return $this->sendData();
    }
}