package main

import (
    "encoding/json"
    "log"
    "net/http"
    "fmt"
)

func callback(w http.ResponseWriter, r *http.Request) {
	
	 // define custom type
	 type RestDataJson struct {
        Type        string  `json:"type"`
        Orderid       string  `json:"orderid"`
		Id   string    `json:"id"`
		Status       string  `json:"status"`
        Signature   string    `json:"signature"`
    }

    // define a var 
    var rData RestDataJson

    err := json.NewDecoder(r.Body).Decode(&rData)
    if err != nil {
        w.WriteHeader(400)
        fmt.Fprintf(w, "Decode error! please check your JSON formating.")
        return
    }

    // Do Someting Or Save Into DB
	fmt.Fprintf(w, "ID: %s", rData.Id)
	fmt.Fprintf(w, "Type: %s", rData.Type)
	fmt.Fprintf(w, "Orderid: %s", rData.Orderid)
	fmt.Fprintf(w, "Status: %s", rData.Status)
	fmt.Fprintf(w, "Signature: %s", rData.Signature)
}

func main() {
    http.HandleFunc("/callback", callback)
    log.Fatal(http.ListenAndServe(":8888", nil))
}