﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace testcsapi.Controllers
{
    [ApiController]
    // POST: /callback
    [Route("[controller]")]
    public class CallbackController : ControllerBase
    {

        [HttpPost]

        public string Main([FromBody] dynamic raw)
        {
            string jsonString = JsonSerializer.Serialize(raw);
            var newJson = JsonSerializer.Deserialize<Dictionary<string, string>>(jsonString);

            //Do Someting Or Save Into DB
            Console.Write("type = " + newJson["type"]);
            Console.Write("orderid = " + newJson["orderid"]);
            Console.Write("id = " + newJson["id"]);
            Console.Write("status = " + newJson["status"]);
            Console.Write("signature = " + newJson["signature"]);

            return jsonString;
        }
    }
}
