var express = require('express')
var bodyParser = require('body-parser')
var app = express()

// parse application/json
app.use(bodyParser.json())

app.post('/callback', function (req, res) {

    // Do Someting Or Save Into DB
    res.end(`
    type = ${req.body.type}
    orderid = ${req.body.orderid}
    id = ${req.body.id}
    status = ${req.body.status}
    signature = ${req.body.signature}
    `)

})

app.listen(process.env.PORT || 8888, () => {
    console.log(`Example app listening on ${process.env.PORT || 8888} port!`);
});