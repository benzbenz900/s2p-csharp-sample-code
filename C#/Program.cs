﻿using System;
using RestSharp;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
namespace dotnet
{
    class Program
    {
        public static Boolean EVALUMENT_DEV = true;
        public static String DEV_URL_API = "https://api.banktransfer.shop2pay.dev/api/";
        public static String PROD_URL_API = "https://api.real-pay.co/api/";

        public static String authorization = "UsJ4rZOgA5LzeoRvudAoZOfLqsidBhC7";
        public static String secertkey = "nVAUsNJ1RQdNzZruXTkUlgnxMcdzuvgEc6KMLTpBPNc6SeNoNB3wL2dH986YRgjt";
        static void Main()
        {
            // autoqrcode
            Dictionary<string, object> autoqrcode_payload = new Dictionary<string, object>();

            autoqrcode_payload.Add("orderid", "string");
            autoqrcode_payload.Add("account", "string");
            autoqrcode_payload.Add("price", 0);
            autoqrcode_payload.Add("from_account", "string");
            autoqrcode_payload.Add("from_bank", "BBL");
            autoqrcode_payload.Add("from_name", "string");

            autoqrcode(autoqrcode_payload);


            //withdraw
            Dictionary<string, object> withdraw_payload = new Dictionary<string, object>();

            withdraw_payload.Add("orderid", "string");
            withdraw_payload.Add("account", "string");
            withdraw_payload.Add("price", 0);
            withdraw_payload.Add("to_banking", "BBL");
            withdraw_payload.Add("name", "string");

            withdraw(withdraw_payload);

            //deposit
            Dictionary<string, object> deposit_payload = new Dictionary<string, object>();

            deposit_payload.Add("orderid", "string");
            deposit_payload.Add("account", "string");
            deposit_payload.Add("price", 0);
            deposit_payload.Add("from_account", "string");
            deposit_payload.Add("from_bank", "BBL");
            deposit_payload.Add("from_name", "string");

            deposit(deposit_payload);

        }

        static void autoqrcode(Dictionary<string, object> payload)
        {
            var data = new Dictionary<string, object>();
            data.Add("url", getURLForSend("autoqrcode"));
            data.Add("playload", genSignature(payload));

            sendData(data);
        }

        static void deposit(Dictionary<string, object> payload)
        {
            var data = new Dictionary<string, object>();
            data.Add("url", getURLForSend("deposit"));
            data.Add("playload", genSignature(payload));

            sendData(data);
        }

        static void withdraw(Dictionary<string, object> payload)
        {
            var data = new Dictionary<string, object>();
            data.Add("url", getURLForSend("withdraw"));
            data.Add("playload", genSignature(payload));

            sendData(data);
        }

        static String getURLForSend(String endpoint)
        {
            if (EVALUMENT_DEV == true)
            {
                return DEV_URL_API + endpoint;
            }
            else
            {
                return PROD_URL_API + endpoint;
            }
        }

        static void sendData(Dictionary<string, object> data)
        {
            var DataPayload = JsonConvert.SerializeObject(data["playload"]);

            var client = new RestClient(data["url"].ToString());
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("authorization", authorization);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", DataPayload, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);


            Console.WriteLine(response.Content);

        }

        static Dictionary<string, object> genSignature(Dictionary<string, object> payload)
        {
            var list = payload.Keys.ToList();
            list.Sort();

            var new_payload = new Dictionary<string, object>();

            foreach (var key in list)
            {
                new_payload.Add(key, payload[key]);
            }

            String signature = Md5Hash(ToQueryString(new_payload) + "&key=" + secertkey);

            new_payload.Add("signature", signature);

            return new_payload;
        }

        public static string ToQueryString(Dictionary<string, object> dict)
        {
            var list = new List<string>();
            foreach (var item in dict)
            {
                list.Add(item.Key + "=" + item.Value);
            }
            return string.Join("&", list);
        }

        static string Md5Hash(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                return sBuilder.ToString();
            }
        }
    }
}
